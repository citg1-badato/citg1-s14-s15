package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.services.PostService;
import com.zuitt.discussion.services.PostServiceImpl;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//fixes cors error enables cross origin request via @CrossOrigin.
@CrossOrigin
public class PostController {
    @Autowired
    PostService postServices;

    // Create Post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        postServices.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully.", HttpStatus.CREATED);
    }


}