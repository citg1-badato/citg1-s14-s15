package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//allows us to use crud repository methods
@Service
public class PostServiceImpl implements PostService {

    //@autowired allows us to use an r as if it was an instance of an object
    @Autowired
    private PostRepository postRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    // Create post
    public void createPost(String stringToken, Post post) {


//Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }
}
